
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Meiai Hostel 2014</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </ul>
                </div>
                <div class="col-md-4">
                    <span class="copyright">+ 55 21 3495-4481 / meiai@meiai.com.br</span>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/localization/messages_pt_BR.js"></script>
    <script src="vendor/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="vendor/skdslider/src/skdslider.min.js"></script>

    <!-- Mapa -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=pt-BR"></script>
    <script src="//cdn.jsdelivr.net/gmap3/6.0.0/gmap3.min.js"></script>
    <script src="js/meiai.mapa.js"></script>

    <script src="js/meiai.js"></script>
    <script src="js/meiai.contato.js"></script>
</body>

</html>