<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Site do Hostel Meiai, localizado em Botafogo, Rio de Janeiro, com suítes e quartos coletivos">
    <meta name="keywords" content="Hostel, Hotel, Albergue, Rio de Janeiro, Rio, Botafogo">
    <meta name="author" content="Pedro Daltro">
    <meta name="robots" content="index, follow">

    <title>Meiai Hostel Botafogo Rio de Janeiro - Brazil</title>

    <!-- Bootstrap Core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/datepicker/css/datepicker.css" rel="stylesheet">

    <!-- Slider -->
    <link href="vendor/skdslider/src/skdslider.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/meiai.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav id="navbar-principal" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll menu_mobile">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<a class="navbar-brand page-scroll" href="#page-top">Start Bootstrap</a> -->
                <a class="navbar-brand" rel="home" href="#" title="O hostel que você estava procurando">
			        <img style="max-width:105px; margin-top: -11px;"
			        	src="img/meiai.png">
			    </a>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#meiai">Meiai</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#reserva">Reservas</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#quartos">Quartos</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#localizacao">Localização</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#guilhermina">Guilhermina</a>
                    </li>
                    <li>
                        <a class="page-scroll btn-page-contato" href="#contato">Contato</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header id="home">

        <!-- Slider -->
        <div class="skdslider">
            <ul id="skdslider-home" class="slides">
                <li>
                    <img src="vendor/skdslider/slides/1.jpg" alt="">
                    <div class="intro-text">
                        <a href="index-english.php" class="page-scroll btn btn-xl idioma eng">English</a>
                        <a href="index-espanol.php" class="page-scroll btn btn-xl idioma spa">Español</a>
                        <div class="intro-heading">Bem-vindo ao Meiai</div>
                        <a href="#reserva" class="page-scroll btn btn-xl"> <i class="fa fa-calendar" style="display: inline-block; margin-right: 16px;"></i> Faça sua reserva</a>
                    </div>
                </li>

                <li>
                    <img src="vendor/skdslider/slides/5.jpg" alt="">
                    <div class="intro-text">
                        <a href="index-english.php" class="page-scroll btn btn-xl idioma eng">English</a>
                        <a href="index-espanol.php" class="page-scroll btn btn-xl idioma spa">Español</a>
                        <div class="intro-heading">Bem-vindo ao Meiai</div>
                        <a href="#reserva" class="page-scroll btn btn-xl"> <i class="fa fa-calendar" style="display: inline-block; margin-right: 16px;"></i> Faça sua reserva</a>
                    </div>
                </li>

                <li>
                    <img src="vendor/skdslider/slides/4.jpg" alt="">
                    <div class="intro-text">
                        <a href="index-english.php" class="page-scroll btn btn-xl idioma eng">English</a>
                        <a href="index-espanol.php" class="page-scroll btn btn-xl idioma spa">Español</a>
                        <div class="intro-heading">Bem-vindo ao Meiai</div>
                        <a href="#reserva" class="page-scroll btn btn-xl"> <i class="fa fa-calendar" style="display: inline-block; margin-right: 16px;"></i> Faça sua reserva</a>
                    </div>
                </li>

                <li>
                    <img src="vendor/skdslider/slides/2.jpg" alt="">
                    <div class="intro-text">
                        <a href="index-english.php" class="page-scroll btn btn-xl idioma eng">English</a>
                        <a href="index-espanol.php" class="page-scroll btn btn-xl idioma spa">Español</a>
                        <div class="intro-heading">Bem-vindo ao Meiai</div>
                        <a href="#reserva" class="page-scroll btn btn-xl"> <i class="fa fa-calendar" style="display: inline-block; margin-right: 16px;"></i> Faça sua reserva</a>
                    </div>
                </li>

                <li>
                    <img src="vendor/skdslider/slides/3.jpg" alt="">
                    <div class="intro-text">
                        <a href="index-english.php" class="page-scroll btn btn-xl idioma eng">English</a>
                        <a href="index-espanol.php" class="page-scroll btn btn-xl idioma spa">Español</a>
                        <div class="intro-heading">Bem-vindo ao Meiai</div>
                        <a href="#reserva" class="page-scroll btn btn-xl"> <i class="fa fa-calendar" style="display: inline-block; margin-right: 16px;"></i> Faça sua reserva</a>
                    </div>
                </li>

                <li>
                    <img src="vendor/skdslider/slides/6.jpg" alt="">
                    <div class="intro-text">
                        <a href="index-english.php" class="page-scroll btn btn-xl idioma eng">English</a>
                        <a href="index-espanol.php" class="page-scroll btn btn-xl idioma spa">Español</a>
                        <div class="intro-heading">Bem-vindo ao Meiai</div>
                        <a href="#reserva" class="page-scroll btn btn-xl"> <i class="fa fa-calendar" style="display: inline-block; margin-right: 16px;"></i> Faça sua reserva</a>
                    </div>
                </li>

            </ul>
        </div>
    </header>