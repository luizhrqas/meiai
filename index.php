<?php require_once "header.php"; ?>

<?php require_once "modal.php"; ?>

    <!-- Services Section -->
    <section id="meiai">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Por que escolher o Meiai?</h2>
                    <h3 class="section-subheading text-muted">Ah, essa é fácil ; )</h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <img src="img/map.png" class="img-responsive icone_pq" alt="Responsive image">
                   
                    <h4 class="service-heading">Localização</h4>
                    <p class="text-muted">Localizado em uma linda rua arborizada de Botafogo, o Meiai é perto das praias, da Lagoa Rodrigo de Freitas, do metrô e de pontos de ônibus que te levam para todos os cantos da cidade.</p>
                </div>
                <div class="col-md-4">
                    <img src="img/casa.png" class="img-responsive icone_pq" alt="Responsive image">
                    <h4 class="service-heading">Estrutura</h4>
                    <p class="text-muted">Fundado em 2012 em uma linda casinha, o Meiai conserva seu cheirinho de novo. A atenção aos detalhes vai dos quartos aos banheiros coletivos. Veja as fotos e conheça mais</p>
                </div>
                <div class="col-md-4">
                    <!--<span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                    -->
                    <img src="img/estrela.png" class="img-responsive icone_pq" alt="Responsive image">
                    <h4 class="service-heading">Aprovado e elogiado</h4>
                    <p class="text-muted">Ninguém melhor que nossos hóspedes para falar das qualidades do Meiai. Nossa nota no Booking é 9,1 mas ainda achamos pouco e continuamos fazendo de tudo para melhorar.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- RESERVA -->
    <section id="reserva">
        <div class ="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2>Faça agora mesmo a sua Reserva</h2>
                </div>
            </div>
            <div class="checkin-checkout">
                <div class="row">
                    <div class="col-xs-5 col-xs-offset-1">
                        <input type="text" name="checkin" placeholder="Check-in" class="form-control input-lg input-1" />
                    </div>
                    <div class="col-xs-5">
                        <input type="text" name="checkout" placeholder="Check-out" class="form-control input-lg input-2" />
                    </div>
                    <div class="col-xs-1">
                        <button type="submit" class="btn-reserva btn btn-lg btn-success">OK</button>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="alerta-sucesso alert text-center alert-success hide">
                        </div>
                        <div class="alerta-erro alert text-center alert-danger hide">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- FOTOS -->
    <section id="quartos" class="bg-amarelo">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Suítes e Quartos coletivos</h2>
                    <h3 class="section-subheading text-muted">Conheça nossos quartos, nós temos acomodações para todos os gostos. Clique para fazer sua reserva.</h3>
                </div>
            </div>
            <!-- SUITES QUARTOS COLETIVOS -->
            <div class="row">
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#master" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/suite1.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4><strong>Suíte</strong> Master</h4>
                        <p class="text-muted">Nossa suíte presidencial : )</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#casal" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/suite2.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Suíte Casal</h4>
                        <p class="text-muted">Parece hotel</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#c4" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/coletivo1.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Quarto Coletivo C4</h4>
                        <p class="text-muted">Até 4 pessoas</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#c6" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/coletivo1.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Quarto Coletivo C6</h4>
                        <p class="text-muted">Até 6 pessoas</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#c8" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/coletivo1.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Quarto Coletivo C8</h4>
                        <p class="text-muted">Até 8 pessoas</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#c14" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/coletivo1.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Quarto Coletivo C14</h4>
                        <p class="text-muted">Até 14 pessoas</p>
                    </div>
                </div>
            </div>

			<!-- ÁREAS COMUNS -->
			<br>
			<br>
			<br>
			<div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Áreas Comuns</h2>
                    <h3 class="section-subheading text-muted">O Meiai possui para uso coletivo 4 banheiros individuais e 2 banheiros coletivos cabinados. Possui ainda um terraço com espreguiçadeiras, chuveirão, pia de apoio e uma churrasqueira. A área de convivência possui dois computadores com acesso a Internet gratuito, livros e jogos.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a class="portfolio-link" data-toggle="modal">
                        
                        <img src="img/areas_coletivas_0.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4><strong>Sala</h4>
                        <p class="text-muted">Dois computadores...</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a class="portfolio-link" data-toggle="modal">
                        
                        <img src="img/areas_coletivas_1.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4><strong>Sala</h4>
                        <p class="text-muted">E muito verde do lado de fora</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a class="portfolio-link" data-toggle="modal">
                        
                        <img src="img/areas_coletivas_2.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4><strong>Salão</h4>
                        <p class="text-muted">asasasasasa</p>
                    </div>
                </div>
               <div class="col-md-4 col-sm-6 portfolio-item">
                    <a class="portfolio-link" data-toggle="modal">
                        
                        <img src="img/areas_coletivas_4.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4><strong>Banheiros Coletivos</h4>
                        <p class="text-muted">3 banheiros, lindos e limpos</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a class="portfolio-link" data-toggle="modal">
                        
                        <img src="img/areas_coletivas_3.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4><strong>Terraço</h4>
                        <p class="text-muted">Aqui entra foto do terraço </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a class="portfolio-link" data-toggle="modal">
                        
                        <img src="img/areas_coletivas_5.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4><strong>Casa</h4>
                        <p class="text-muted">Gostou da nossa casinha?</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section id="localizacao">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Localização</h2>
                    <h3 class="section-subheading text-muted">Nossa casinha fica em uma linda e arborizada rua em Botafogo: <mark><strong>Guilhermina Guinle, 127 </strong></mark></h3>
                </div>

            </div>
        </div> <!-- .container -->

        <div id="mapa">
        </div> <!-- #mapa -->

        <div class="container">
        
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/1.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    
                                    <h4 class="subheading">Perto da praia</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">O bairro de botafogo fica do lado de Copacabana. Dá até pra ir a pé (caminhada de 20 minutos). O metrô e os ônibus também te levam para Copacabana, Ipanema e Leblon com todo o conforto.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="subheading">Perto do Lagoa</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">A Lagoa é um dos lugares mais belos do Rio, e você pode ir andando (30 min) ou pegar um ônibus (15 minutos). Ah, o ônibus também deixa no magnífico Jardim Botânico</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/3.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="subheading">Perto do Metrô</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">O metrô fica a 3 quadras do Meiai e te leva para vários lugares bacanas, como praias, Maracanã, Tijuca (como tem bar bom na Tijuca!), Aterro do Flamengo, Centro Histórico da Cidade.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/botafogo.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="subheading">Botasoho</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Botafogo está tão na moda que ganhou até apelido. As opções culturais e gastronômicas fazem de Botafogo a <a href= "http://oglobo.globo.com/rio/botafogo-bairro-que-se-renova-13988916" target= "blank">Soho carioca</a>! Restaurantes, bares, exposições, cinemas, centro cultural e muito mais</p>
                                </div>
                            </div>
                        </li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>

 <!-- Guilhermina Section -->
    <section id="guilhermina">

        <div class="container">

            <div class="col-xs-8 col-sm-6 col-md-4 col-lg-4 col-xs-offset-2 col-sm-offset-3 col-md-offset-4 col-lg-offset-4 box_g">
                
                <img class="img-responsive" src="img/logo_gui.png" alt="">
                
            </div> <!-- fim do lgs offset box_g -->

			<div class="clearfix"></div>

                <div class="row">
                    <div class="box_g_branco">                        
                    	<h3>
                    		O Meiai também conta com um delicioso restaurante para você
                    	</h3>
                    </div>
                </div>

                <div class="col-xs-8 col-sm-6 col-md-4 col-lg-4 col-xs-offset-2 col-sm-offset-3 col-md-offset-4 col-lg-offset-4 box_g_site">
                
                    <h4>Clique e visite o site</h4>

               </div> <!-- fim do lgs offset box_g -->
            
        </div>
    </section>

<!-- Barra preta divisória -->
    <aside class="clients bg-preto text-center">
        <div class="container">
            <div class="row funcionamento">
              
                <br>
              
            </div>
        </div>
    </aside>

 <!-- FIM GUILHERMINA SECTION -->

    <section id="contato">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Entre em contato com o MEIAI</h2>
                    <h3 class="section-subheading">Nós prometemos responder rapidinho : ) </h3>  <!-- classe text muted, TIREI -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Seu nome" id="name" required>
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Seu E-mail" id="email" required>
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Seu telefone (opcional)" id="phone">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Sua mensagem" id="message" required></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Enviar Mensagem</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php require_once "footer.php"; ?>