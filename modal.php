<!-- QUARTOS E SUÍTES COLETIVAS -->

<!-- MASTER -->
<div class="portfolio-modal modal fade" id="master" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Suíte Master</h2>
                        <p class="item-intro text-muted">Nossa “suíte presidencial” : )</p>
                        <img class="img-responsive" src="img/popup_quartos.png" alt="">
                        <p> Cabem até 4 hóspedes </p>
                        <p> A suíte Master oferece ar condicionado split (sem barulho!), televisão LCD (com TV a cabo) e banheiro privativo, com todo o conforto que você merece. Ah, também tem wi-fi</p>
                        
                        <strong><mark class="marcatexto">Diária: R$250</mark></strong>. Entre em <a href="#contact" class="btn-ir-contato" data-dismiss="modal">contato</a> e consulte nossos preços para feriados e datas especiais.</p>

                        <br>

                        <!--<button><a href:"#reserva" type="button" class="btn-lg btn-primary" data-toggle="modal"><i class="fa fa-check"></i> Fazer reserva </a></button> -->

                        <!-- <a href="#reserva" class="btn btn-lg btn-primary" data-toggle="modal"><i class="fa fa-check"></i>  Fazer reserva</a> -->

                        <!-- RESERVAS -->

                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4>Faça agora mesmo a sua Reserva</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-5 col-xs-offset-1">
                                <input type="text" name="checkin" placeholder="Check-in" class="form-control input-lg" id="dpd1" />
                            </div>
                            <div class="col-xs-5">
                                <input type="text" name="checkout" placeholder="Check-out" class="form-control input-lg" id="dpd2" />
                            </div>
                            <div class="col-xs-1">
                                <button type="submit" id="btn-reserva" class="btn btn-lg btn-success">OK</button>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-xs-8 col-xs-offset-2">
                                <div id="alerta-sucesso" class="alert text-center alert-success hide">
                                </div>
                                <div id="alerta-erro" class="alert text-center alert-danger hide">
                                </div>
                            </div>
                        </div>

                    <!-- RESERVAS -->

                    <br>

                        <button type="button" class="btn btn-danger fechar" data-dismiss="modal"><i class="fa fa-times"></i> Voltar </button>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- CASAL -->
<div class="portfolio-modal modal fade" id="casal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Suíte Casal</h2>
                        <p class="item-intro text-muted">Nossa “suíte presidencial” : )</p>
                        <img class="img-responsive" src="img/popup_quartos.png" alt="">
                        <p> Cabem até 4 hóspedes </p>
                        <p> A suíte Master oferece ar condicionado split (sem barulho!), televisão LCD (com TV a cabo) e banheiro, com todo o conforto que você merece. Ah, também tem wi-fi</p>
                        
                        <strong><mark class="marcatexto">Diária: R$250</mark></strong>. Entre em <a href="#contact" class="btn-ir-contato" data-dismiss="modal">contato</a> e consulte nossos preços para feriados e datas especiais.</p>

                        <!-- <a href="#reserva" class="btn btn-lg btn-primary" data-toggle="modal"><i class="fa fa-check"></i>  Fazer reserva</a> -->

                        <!-- RESERVAS -->

                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4>Faça agora mesmo a sua Reserva</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-5 col-xs-offset-1">
                                <input type="text" name="checkin" placeholder="Check-in" class="form-control input-lg" id="dpd1" />
                            </div>
                            <div class="col-xs-5">
                                <input type="text" name="checkout" placeholder="Check-out" class="form-control input-lg" id="dpd2" />
                            </div>
                            <div class="col-xs-1">
                                <button type="submit" id="btn-reserva" class="btn btn-lg btn-success">OK</button>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-xs-8 col-xs-offset-2">
                                <div id="alerta-sucesso" class="alert text-center alert-success hide">
                                </div>
                                <div id="alerta-erro" class="alert text-center alert-danger hide">
                                </div>
                            </div>
                        </div>

                    <!-- fim RESERVAS -->

                        <button type="button" class="btn btn-danger fechar" data-dismiss="modal"><i class="fa fa-times"></i> Voltar </button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



 <!-- C4 -->
<div class="portfolio-modal modal fade" id="c4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Quarto Coletivo C4</h2>
                        <p class="item-intro text-muted">Cabem até 4 pessoas</p>
                        <img class="img-responsive" src="img/c8_popup.png" alt="">
                        <p>Cabem até 4 hóspedes, que desfrutarão de ar condicionado split , locker individual em cada cama, 4 banheiros individuais e 2 banheiros coletivos cabinados espalhados pelo hostel e wi-fi.</p>
                        
                        <strong><mark class="marcatexto">Diária: R$250</mark></strong>. Entre em <a href="#contact" class="btn-ir-contato">contato</a> e consulte nossos preços para feriados e datas especiais.</p>

                        <!-- <a href="#reserva" class="btn btn-lg btn-primary" data-toggle="modal"><i class="fa fa-check"></i>  Fazer reserva</a> -->

                        <!-- RESERVAS -->

                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4>Faça agora mesmo a sua Reserva</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-5 col-xs-offset-1">
                                <input type="text" name="checkin" placeholder="Check-in" class="form-control input-lg" id="dpd1" />
                            </div>
                            <div class="col-xs-5">
                                <input type="text" name="checkout" placeholder="Check-out" class="form-control input-lg" id="dpd2" />
                            </div>
                            <div class="col-xs-1">
                                <button type="submit" id="btn-reserva" class="btn btn-lg btn-success">OK</button>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-xs-8 col-xs-offset-2">
                                <div id="alerta-sucesso" class="alert text-center alert-success hide">
                                </div>
                                <div id="alerta-erro" class="alert text-center alert-danger hide">
                                </div>
                            </div>
                        </div>

                    <!-- fim RESERVAS -->

                        <button type="button" class="btn btn-danger fechar" data-dismiss="modal"><i class="fa fa-times"></i> Voltar </button>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- C6 -->
<div class="portfolio-modal modal fade" id="c6" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Quarto Coletivo C6</h2>
                        <p class="item-intro text-muted">Cabem até 6 pessoas</p>
                        <img class="img-responsive" src="img/c8_popup.png" alt="">
                        <p>Cabem até 6 hóspedes, que desfrutarão de ar condicionado split , locker individual em cada cama, 4 banheiros individuais e 2 banheiros coletivos cabinados espalhados pelo hostel e wi-fi.</p>
                        
                        <strong><mark class="marcatexto">Diária: R$250</mark></strong>. Entre em <a href="#contact" class="btn-ir-contato">contato</a> e consulte nossos preços para feriados e datas especiais.</p>

                        <!-- <a href="#reserva" class="btn btn-lg btn-primary" data-toggle="modal"><i class="fa fa-check"></i>  Fazer reserva</a> -->

                        <!-- RESERVAS -->

                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4>Faça agora mesmo a sua Reserva</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-5 col-xs-offset-1">
                                <input type="text" name="checkin" placeholder="Check-in" class="form-control input-lg" id="dpd1" />
                            </div>
                            <div class="col-xs-5">
                                <input type="text" name="checkout" placeholder="Check-out" class="form-control input-lg" id="dpd2" />
                            </div>
                            <div class="col-xs-1">
                                <button type="submit" id="btn-reserva" class="btn btn-lg btn-success">OK</button>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-xs-8 col-xs-offset-2">
                                <div id="alerta-sucesso" class="alert text-center alert-success hide">
                                </div>
                                <div id="alerta-erro" class="alert text-center alert-danger hide">
                                </div>
                            </div>
                        </div>

                    <!-- fim RESERVAS -->

                        <button type="button" class="btn btn-danger fechar" data-dismiss="modal"><i class="fa fa-times"></i> Voltar </button>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- C8 -->
<div class="portfolio-modal modal fade" id="c8" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Quarto Coletivo C8</h2>
                        <p class="item-intro text-muted">Cabem até 8 pessoas</p>
                        <img class="img-responsive" src="img/c8_popup.png" alt="">
                        <p>Cabem até 8 hóspedes, que desfrutarão de ar condicionado split , locker individual em cada cama, 4 banheiros individuais e 2 banheiros coletivos cabinados espalhados pelo hostel e wi-fi.</p>
                        
                        <strong><mark class="marcatexto">Diária: R$250</mark></strong>. Entre em <a href="#contact" class="btn-ir-contato">contato</a> e consulte nossos preços para feriados e datas especiais.</p>

                        <!-- <a href="#reserva" class="btn btn-lg btn-primary" data-toggle="modal"><i class="fa fa-check"></i>  Fazer reserva</a> -->

                        <!-- RESERVAS -->

                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4>Faça agora mesmo a sua Reserva</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-5 col-xs-offset-1">
                                <input type="text" name="checkin" placeholder="Check-in" class="form-control input-lg" id="dpd1" />
                            </div>
                            <div class="col-xs-5">
                                <input type="text" name="checkout" placeholder="Check-out" class="form-control input-lg" id="dpd2" />
                            </div>
                            <div class="col-xs-1">
                                <button type="submit" id="btn-reserva" class="btn btn-lg btn-success">OK</button>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-xs-8 col-xs-offset-2">
                                <div id="alerta-sucesso" class="alert text-center alert-success hide">
                                </div>
                                <div id="alerta-erro" class="alert text-center alert-danger hide">
                                </div>
                            </div>
                        </div>

                    <!-- fim RESERVAS -->

                        <button type="button" class="btn btn-danger fechar" data-dismiss="modal"><i class="fa fa-times"></i> Voltar </button>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- C14 -->
<div class="portfolio-modal modal fade" id="c14" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Quarto Coletivo C14</h2>
                        <p class="item-intro text-muted">Cabem até 4 pessoas</p>
                        <img class="img-responsive" src="img/c8_popup.png" alt="">
                        <p>Cabem até 14 hóspedes, que desfrutarão de ar condicionado split , locker individual em cada cama, 4 banheiros individuais e 2 banheiros coletivos cabinados espalhados pelo hostel e wi-fi.</p>
                        
                        <strong><mark class="marcatexto">Diária: R$250</mark></strong>. Entre em <a data-dismiss= "modal" href="#contact" class="btn-ir-contato">contato</a> e consulte nossos preços para feriados e datas especiais.</p>

                        <!-- <a href="#reserva" class="btn btn-lg btn-primary" data-toggle="modal"><i class="fa fa-check"></i>  Fazer reserva</a> -->

                        <!-- RESERVAS -->

                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4>Faça agora mesmo a sua Reserva</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-5 col-xs-offset-1">
                                <input type="text" name="checkin" placeholder="Check-in" class="form-control input-lg" id="dpd1" />
                            </div>
                            <div class="col-xs-5">
                                <input type="text" name="checkout" placeholder="Check-out" class="form-control input-lg" id="dpd2" />
                            </div>
                            <div class="col-xs-1">
                                <button type="submit" id="btn-reserva" class="btn btn-lg btn-success">OK</button>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-xs-8 col-xs-offset-2">
                                <div id="alerta-sucesso" class="alert text-center alert-success hide">
                                </div>
                                <div id="alerta-erro" class="alert text-center alert-danger hide">
                                </div>
                            </div>
                        </div>

                    <!-- fim RESERVAS -->

                        <button type="button" class="btn btn-danger fechar" data-dismiss="modal"><i class="fa fa-times"></i> Voltar </button>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>