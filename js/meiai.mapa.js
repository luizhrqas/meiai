$(document).ready(function() {

	$('#mapa').gmap3({
	 map: {
	    options: {
	      maxZoom: 16,
	      scrollwheel: false
	    }  
	 },
	 marker:{
	    address: "Rua Guilhermina Guinle, 127, Botafogo, Rio de Janeiro, RJ, Brasil"
	 }
	},
	"autofit" );

});