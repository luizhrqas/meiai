/*!
 * Start Bootstrap - Agnecy Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('body').on('click', 'a.page-scroll', function(event) {
        var $anchor = $(this);

        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        
        event.preventDefault();
    });

    $(".btn-ir-contato").click(function() {
    	$(".close-modal,.btn-page-contato").click();
    });

    $('.navbar-fixed-top').on('activate.bs.scrollspy', function () {
        $(".navbar-fixed-top").addClass("scrollspy-activated");
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    if($("#skdslider-home li").length > 1) {
        var options = {
            delay: 7000,
            animationSpeed: 1500,
            showNextPrev: true,
            showPlayButton: false,
            autoSlide: true,
            animationType: 'fading'
        };
    } else {
        var options = {
            delay: 7000,
            animationSpeed: 1500,
            showNextPrev: false,
            showPlayButton: false,
            autoSlide: false,
            animationType: 'fading'
        };
    }

    $('#skdslider-home').skdslider(options);

    $(".checkin-checkout").each(function(index, element) {

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $(element).find(".input-1").datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          },
          format: 'dd/mm/yyyy'
        }).data('datepicker');

        $(element).find(".input-1").on('changeDate', function(ev) {

          var checkout_date = $(element).find(".input-2").data("datepicker").date;
          
          if (ev.date.valueOf() > checkout_date.valueOf()) {
            var newDate = new Date(ev.date)
            
            newDate.setDate(newDate.getDate() + 1);
            
            checkout.setValue(newDate);
          }

          checkin.hide();

          $(element).find(".input-2").focus();

        }).data('datepicker');

        var checkout = $(element).find(".input-2").datepicker({
          onRender: function(date) {
            
            var checkin_date = $(element).find(".input-1").data("datepicker").date;

            return date.valueOf() <= checkin_date.valueOf() ? 'disabled' : '';
          },
          format: 'dd/mm/yyyy'
        }).data('datepicker');

        $(element).find(".input-2").on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');

    });

    $("body").on("click", ".btn-reserva", function() {

        var checkin_checkout = $(this).parent().parent().parent();

        var checkin = checkin_checkout.find(".input-1").val();
        var checkout = checkin_checkout.find(".input-2").val();
        var mensagem = "";

        if(checkin.length == 0) {
            var mensagem = mensagem + " Check-in vazio.";
        }

        if(checkout.length == 0) {
            var mensagem = mensagem + " Check-out vazio.";
        }

        if(checkin.length == 0 || checkout.length == 0) {
            checkin_checkout.find(".alerta-sucesso,.alerta-erro").addClass("hide");

            var tipo = "erro";
        }

        if(checkin.length > 0 && checkout.length > 0) {
            checkin_checkout.find(".alerta-sucesso,.alerta-erro").addClass("hide");

            var mensagem = "Aguarde um instante...";
            var tipo = "sucesso";
        }

        checkin_checkout.find(".alerta-" + tipo).removeClass("hide").html(mensagem);

        if(tipo == "sucesso") {

            $.ajax({
                type: "POST",
                url: "ajax-reserva.php",
                data: {dt1: checkin, dt2: checkout},
                success: function(data) {

                    checkin_checkout.find(".alerta-sucesso, .alerta-erro").addClass("hide");
                    checkin_checkout.find(".alerta-sucesso").removeClass("hide").html(data);
                },
                failure: function() {
                    alert("Erro ao carregar reserva! Tente novamente mais tarde.");

                    checkin_checkout.find(".alerta-sucesso, .alerta-erro").addClass("hide");
                }
            });
        }
    });

});