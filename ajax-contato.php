<?php 

/**
 * Quem irá receber os e-mails.
 */
$destinatarios = array(
	'meiai@meiai.com.br',
	'joao@meiai.com.br',
	'gerencia@meiai.com.br'
);

/**
 * Informações do formulário.
 */
$name = $_POST['name'];
$email_address = $_POST['email'];
$phone = $_POST['phone'];
$message = $_POST['message'];

/**
 * Tratamento dos campos.
 */
$email_body = "
<strong>Nome:</strong> $name<br />
<strong>E-mail:</strong> $email_address<br />
<strong>Telefone (opcional):</strong> $phone<br />
<strong>Mensagem:</strong> $message<br />
<strong>Horário:</strong> " . date('d/m/Y H:i:s') . "<br />";

/**
 * Disparo.
 */
require_once "vendor/phpmailer/PHPMailerAutoload.php";

$mail = new PHPMailer;

$mail->CharSet = 'UTF-8';
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.mandrillapp.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'luizhrqas@gmail.com';                 // SMTP username
$mail->Password = '52Rq1rYS9GGoOlWVRbIo7Q';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;

$mail->From = 'no-reply@meiai.com.br';
$mail->FromName = 'Meiai Hostel';

foreach($destinatarios as $d) {
	$mail->addAddress($d);
}

$mail->addReplyTo('meiai@meiai.com.br', 'Meiai Hostel');

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = '[Meiai Hostel] Contato do Site';
$mail->Body    = $email_body;
$mail->AltBody    = $email_body;

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}

return true;
?>